<?php
       
namespace AppBundle\Entity;
       
use FOS\UserBundle\Model\User;
use Doctrine\ORM\Mapping as ORM;
       
/**
* @ORM\Entity
* @ORM\Table()
*/
class Utente extends User
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;       
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $dataRegistrazione;       
           
    public function __construct(){
        parent::__construct();
        $this->dataRegistrazione = new \DateTime();
    }
       
}
